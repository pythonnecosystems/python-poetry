# Python Poetry <sup>[1](#footnote_1)</sup>

Python 프로젝트에서 pip와 venv를 사용해 종속성을 관리해 본 적이 있다면 모든 것을 일관된 상태로 유지하는 단계에 이미 익숙할 것입니다. 이전에 [Python 가상 환경](https://medium.com/@pdx.lucasm/python-virtual-environments-18ee3e8d2c3f)과 [pip의 이해](https://medium.com/@pdx.lucasm/understanding-pip-the-package-installer-for-python-d3401de7072a)에서 이에 대해 작성한 적이 있다. 이때 requirements.txt 파일, 가상 환경 및 그룹 종속성(예: 개발 종속성, CI/CD 종속성 등)을 관리하는 것이 매우 지루하다. 너무 지루해서 Python 개발자들은 이러한 집안일을 처리하기 위해 새로운 의존성 관리자를 만들기로 결정했습. 이 포스팅에서는 의존성 관리, 코드 패키징 및 게시를 위해 Python 커뮤니티에서 매우 인기 있는 도구인 **Poetry**에 대해 설명한다.

<a name="footnote_1">1</a>: 이 페이지는 [Python Poetry](https://medium.com/@pdx.lucasm/python-poetry-f8cbab0eef94)를 편역한 것이다.
