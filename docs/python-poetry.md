# Python Poetry

## Poetry vs. pip+venv
Poetry가 어떻게 우리의 삶을 더 쉽게 만들어 주는지 이해하기 위해, 개발 주기의 몇 가지 일반적인 작업을 Poetry를 사용한 '표준' 접근 방식과 pip과 venv를 사용한 접근 방식을 비교해 보겠다.

### Poetry 설치
다음과 같이 설치할 디렉토리에 Poetry를 설치한다.

```bash
$ curl -sSL https://install.python-poetry.org | python3 <설치할 디렉토리>
```

설치를 확인한다.

```bash
$ poetry --version
Poetry (version 1.7.1)
```

### 프로젝트 시작
Poetry를 사용하면 Poetry CLI를 통해 간단한 명령을 실행하여 새 프로젝트를 시작할 수 있다.

```bash
# with poetry
$ poetry new my-project
```

Poetry가 없으면 새 폴더를 만들고 이 폴더 안에 가상 환경을 만든 다음 가상 환경을 활성화해야 한다.

```bash
# without poetry
$ mkdir my-project
$ cd 'my-project'
$ python3 -m venv venv
$ source venv/bin/activate
```

Poetry를 사용하면 프로젝트를 더 쉽게 시작할 수 있을 뿐만 아니라 기본적인 프로젝트 구조도 만들 수 있다.

```
my-project
├── pyproject.toml
├── README.md
├── my_project
│   └── __init__.py
└── tests
    └── __init__.py
```

Poetry의 프로젝트 폴더에는 "venv" 폴더가 생성되지 않는다는 점에 유의하세요. Poetry는 실제로 프로젝트 폴더 외부의 다른 위치에서 가상 환경을 관리하므로 걱정할 필요가 없다. 다시 말해, 존재하지만 눈에 보이지 않을 뿐이다.

| Criteria | Poetry | pip + venv |
|----------|--------|------------|
| Verbosity | 1 Command | 4 Commands |
| Virtual Environement | Yes | Yes |
| Initial Structure | Basic | None |

### 종속성 설치와 제거
Poetry CLI의 간단한 명령으로 다시 한번 확인해 보세요.

```bash
# with poetry
$ poetry add requests
```

Poetry는 프로젝트의 가상 환경에 라이브러리를 설치하고 종속성 일관성을 보장하기 위해 `project.toml`과 `.lock` 파일도 업데이트한다.

Poetry를 사용하지 않으면 라이브러리를 설치하고 requirements.txt 파일도 업데이트해야 한다 (이미 가상 환경이 활성화되어 있다고 가정).

```bash
# without poetry
(venv) $ pip install requests
(venv) $ pip freeze > requirements.txt
```

패키지를 제거할 때도 마찬가지이다.

```bash
# with poetry
$ poetry remove requests
# without poetry
(venv) $ pip uninstall requests
(venv) $ pip freeze > requirements.txt
```

`lock` 파일을 업데이트하기 위해 명시적으로 작업할 필요 없이 Poetry를 사용하여 종속성을 관리할 수 있다는 점도 좋다.

### 새 컴퓨터에서 프로젝트 설정
컴퓨터에서 프로젝트를 새로 설정하려면 일반적으로 GitHub와 같은 원격 서버에서 git 저장소를 복제한 다음 IDE에서 프로젝트 폴더를 엽니다. 이는 Python 프로젝트가 아닌 경우에도 대부분의 경우 일반적인 절차이다. 여기에서는 VSCode를 IDE로 사용한다.

```bash
# clone the project from remote
$ git clone https://github.com/<some-user-or-org>/<my-repository>.git
$ code "my-repository"
```

이 단계는 Poetry 사용 여부에 관계없이 필수이다. 다음으로 종속성을 설치해야 한다. Poetry를 사용하면 설치하려는 종속성 그룹을 선택할 수 있다. 한편 Poetry를 사용하지 않으면 종속성 그룹을 설치하는 것이 그렇게 간단하지 않다. 실제로 우리는 설정에서 개발 종속성을 설치한다.

```bash
# with poetry
$ poetry install

# without poetry
(venv) $ pip install requirements_dev.txt
# you often will install your root package as well in editable mode
(venv) $ pip install -e .
```

Poetry는 기본적으로 프로젝트 종속성, 개발 종속성 및 프로젝트 자체를 편집 가능한 모드로 설치한다. Poetry를 사용하지 않고 이 동작을 에뮬레이트하려면 `requirements_dev.txt` 파일을 별도로 관리하고 프로젝트 자체를 설치하는 추가 단계를 수행해야 한다.

Poetry를 사용하여 개발 종속성을 추가하는 것은 새 종속성을 포함하는 것만큼이나 쉽다.

```bash
$ poetry add pytest --group dev
```

### 가상 환경에서 명령 실행
이쯤 되면 Poetry가 훌륭한 도구라는 것을 이미 확신하셨겠지만, 가상 환경 내에서 스크립트와 명령을 실행하는 일반적인 작업을 한 가지 더 살펴보겠다.

Poetry의 관리형 가상 환경에서 스크립트를 실행하려면 아래와 같은 명령을 사용한다.

```bash
$ poetry run pytest tests/
```

이 명령은 가상 환경에서 Poetry를 실행한 후 전달한 모든 것을 활성화할 필요 없이 실행한다.

poetry 없이 실행하는 것과 동일한 방법은 가상 환경을 활성화한 후 pytest를 실행하는 것이다.

```bash
(venv) $ source venv/bin/activate
(venv) $ pytest tests/
```

Poetry의 관리형 가상 환경을 수동으로 활성화할 수도 있지만, 하위 셸 내에서 'venv 모드'로 쉽게 진입할 수 있는 명령도 제공한다.

```bash
$ poetry shell
# to deactivate, just enter 'exit' command
```

## 기타 기능
여기에 제시된 명령어에는 모든 Python 개발자를 위한 완벽한 도구가 될 수 있는 많은 하위 명령어와 옵션이 있다. 프로젝트에 대한 특정 요구사항이 있는데 Poetry가 이를 지원할 수 있는지 확실하지 않은 경우, 문서를 빠르게 검색하면 원하는 것을 찾을 수 있을 것이다.

### 기존 프로젝트에 Poetry 설정하기
이제 Poetry에 관심이 생겨서 현재 프로젝트에서 시도해보고 싶은가요? Poetry로 쉽게 전환할 수 있다. 이미 프로젝트가 있고 Poetry를 사용하려면 프로젝트의 루트 디렉터리에서 `poetry init`을 실행하면 된다.

### 패키지 구축하기
라이브러리에서 작업 중이거나 어떤 이유로 패키지의 빌드를 생성해야 하는 경우, Poetry가 도움이 될 수 있다. Poetry로 Python 설치형 패키지를 생성하는 것은 간단히 `poetry build`를 실행하면 된다.

### PyPI 또는 기타 인덱스에 패키지 게시하기
Poetry의 또 다른 장점은 일부 인덱스(기본적으로 PyPI)를 통해 패키지를 배포할 수 있는 `poetry publish` 명령이다. 하지만 이 명령을 사용하기 전에 자격 증명과 인덱스 구성을 설정해야 한다.

### project.toml로 구동
이 구성 파일은 Poetry의 기본 구성 파일이다. 대부분의 경우 이 파일이 Poetry의 유일한 구성 파일(때로는 전체 프로젝트의 구성 파일)이므로 버전 관리 시스템(git 리포지토리)에 보관하는 것이 매우 중요하다. 여기에는 이름, 버전, 설명 등 프로젝트의 메타데이터가 있다. 또한 종속성을 저장하고 여러 라이브러리(예: pytest, tox, precommit 등)의 구성 정보를 저장하는 데 이 파일을 사용한다.

### CI/CD 파이프라인
종속성 그룹 관리, 가상 환경, 사용자 지정 스크립트 및 프로젝트 메타데이터와 같은 기능 덕분에 Poetry는 CI/CD 파이프라인과 통합하기에 좋은 도구이다. 게다가 CLI의 형식은 CI/CD 개발에 매우 적합하다.

### Requirements 파일 생성
Poetry를 사용하면서 requirements.txt 파일을 유지해야 하는 경우 명령을 사용하여 이 작업을 수행할 수 있다.

```bash
$ poetry export -f requirements.txt --output requirements.txt
```

예를 들어 요구 사항 파일에 종속성 그룹을 포함하거나 제외하는 등 이 내보내기 명령을 커스터마이즈하는 데 사용할 수 있는 몇 가지 매개 변수가 있다. 이 기능은 CI/CD 또는 requirement.txt 파일에 의존하는 일부 프레임워크에 유용할 수 있다.

### `.env` 파일의 환경 변수
이것이 Poetry에서 빠진 부분이다. Poetry는 스크립트 시작 시 텍스트 파일에서 환경 변수를 자동으로 로드하는 `.env`(일명 dotenv)를 지원하지 않는다. 하지만 환경 변수가 필요한 경우 스크립트/명령어/어플리케이션을 실행하기 전에 환경 변수를 선언할 수 있다.

### Plugins
Poetry를 사용하면 기능을 확장할 수 있는 플러그인을 만들 수 있다.

`.env` 파일의 경우, Poetry 에코시스템의 일부로 `poetry-dotenv-plugin`이 있다. 이 플러그인은 `.env` 파일을 처리하여 가상 환경에서 스크립트를 실행하기 전에 환경 변수를 로드한다.

> **Note**<br>
> (플러그인이 아닌) `.env`에 대해 자세히 알아보려면 [python-dotenv](https://pypi.org/project/python-dotenv/)를 살펴보세요.

> **Todo**<br>
> Plugin 설치하는 방법 기술 필요

## 마치며
Poetry에는 개발 주기의 파이프라인과 벤더의 어려움을 극복할 수 있는 다양한 기능이 있다. 필자는 프로젝트에서 항상 Poetry를 사용하고 있으며, 특히 라이브러리 개발 작업을 할 때 많은 도움이 된다.

Python 개발자로서 아직 Poetry를 사용해 보지 않았다면 꼭 한번 사용해 보시기 바란다.

## 참고 문헌
- [Poetry](https://python-poetry.org/?source=post_page-----f8cbab0eef94--------------------------------)
- [Dependency Management With Python Poetry — Real Python](https://realpython.com/dependency-management-python-poetry/)
- [Python Poetry: Package and venv Management Made Easy](https://python.land/virtual-environments/python-poetry)
- [How to Create and Use Virtual Environments in Python With Poetry — YouTube](https://www.youtube.com/watch?v=0f3moPe_bhk&t=257s)
- [Alive Poetry Society](https://ctsictai.medium.com/alive-poetry-society-755b80a807ad)
